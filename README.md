# Beti API client
Api client for beti (link to swagger)

## Install
to composer json:
```
"require": {
...
  "maxlen/beti-api-client": "dev-master",
},
"repositories": [
  ...
  {
    "type": "git",
    "url": "https://bitbucket.org/maxlen/beti-api-client.git"
  }
]
```
in console:
```php
composer require maxlen/beti-api-client
```

## For comfortable use
in common/config/params-local.php:
```
'beti-api' => [
    'url' => getenv('BETI_URL') ?: 'https://api.stage5.beti.io',
    'brandId' => getenv('BETI_BRAND') ?: 'vulcangold',//'grandbet',
    'device' => getenv('BETI_DEVICE') ?: 'device',
    'department' => getenv('BETI_DEPARTMENT') ?: 'PLAYER',
]
```

## SignUp
```php
$client = new Client();
$body = [
            'email' => $user->email,
            'password' => $user->password,
//            'brandId' => $this->betiC->parameters->getParam('brandId'),
        ];
        
$client->profile->signup($body);
var_dump($client->parameters->getParam('uuin'));
```

## SignIn SignOut
```php

$client->auth->signin([
  'login' => $email,
  'password' => $password,
  'brandId' => $brandId,
  ]);
var_dump($client->parameters->getParam('token'));

$client->auth->logout();
```

## Get Profile Data
```php
$profileData = (new Client($authParams))->profile->getProfile();
//or
$profileData = $client->profile->getProfile();
```

## Token
```php
$client->auth->refreshToken();
```

## Use for get task status:
```php
$client = new ProxyClient(['serviceVersion' => 'v3']);
$client->command(Command::GET_TASK_STATUS)->taskId(2)->run();
var_dump($client->response->getAllContents();
```


