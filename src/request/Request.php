<?php

namespace Maxlen\BetiClient\request;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Maxlen\BetiClient\exceptions\NetworkException;
use yii\helpers\VarDumper;


/**
 * Класс для выполнения запросов
 *
 * @package Maxlen\BetiClient\request
 */
class Request
{
    private $guzClient;
    private $options;

    /**
     * @var \stdClass
     */
    public $response;

    public function __construct()
    {
        $this->guzClient = new Client([
            'base_uri' => $this->parameters->getParam('url'),
        ]);

        $this->response = new \stdClass();
        $this->response->source = null;
        $this->response->errors = null;
        $this->response->body = null;
    }

    /**
     * @param $path
     * @return mixed
     * @throws NetworkException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function request($path)
    {
        $this->parameters->setParam('path', $path);
        $this->setOptions($this->parameters);

        $result = $this->guzClient->request(
            $this->parameters->getParam('method'),
            $path,
            $this->options
        );

//            $this->response = $result; !!!!!!!!
        $this->response->source = $result;

        if ($result->getStatusCode() != 200) {
            $this->response->body = $result->getBody()->getContents();
            $this->response->errors = $this->parseResponse($result);
        }

        $response = $this->parseResponse($result);

        return $response;
    }

    /**
     * @param $response
     * @return mixed
     */
    public function parseResponse(Response $response)
    {
        return json_decode($response->getBody(), true);
    }

    /**
     * @param Params $parameters
     */
    private function setOptions(Params $parameters)
    {
        $this->options = [
            'headers' => [
                'Content-Type' => 'application/json'
            ],
            'exceptions' => false
        ];

        if (!empty($parameters->getParam('token'))) {
            $this->options['headers']['Authorization'] =
                'Bearer ' . $parameters->getParam('token');
        }

        if (!empty($parameters->getBody())) {
            $this->options['body'] = $parameters->getBody();
        }

        $this->setHeaders($parameters);
    }

    /**
     * @param Params $parameters
     */
    private function setHeaders(Params $parameters)
    {
        if (!empty($parameters->getParam('headers'))) {
            foreach ($parameters->getParam('headers') as $headerName => $headerValue) {
                $this->options['headers'][$headerName] = $headerValue;
            }
        }
    }
}