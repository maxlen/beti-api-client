<?php

namespace Maxlen\BetiClient\request;


class Params
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_DELETE = 'DELETE';

    /**
     * @var array POST данные
     */
    private $postParams = [];

    /**
     * @var string body данные
     */
    private $bodyParams;

    /**
     * @var string method
     */
    private $method = self::METHOD_GET;

    private $url;
    private $path;
    private $login;
    private $password;
    private $token;
    private $uuid;
    private $brandId;
    private $device;
    private $department;
    private $headers = [];

    /**
     * @param string $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param array $params
     * @return $this
     */
    public function setParams(array $params)
    {
        foreach ($params as $key => $val) {
            $this->setParam($key, $val);
        }
        return $this;
    }

    /**
     * @param string $name Имя параметра
     * @param string $value Значение параметра
     * @return $this
     */
    public function setParam($name, $value)
    {
        $this->$name = $value;
        return $this;
    }

    /**
     * @param string $name Имя параметра
     * @return string|null
     */
    public function getParam($name)
    {
        return $this->$name;
    }

    /**
     * @param array $headers
     * @return $this
     */
    public function setHeaders(array $headers)
    {
        foreach ($headers as $key => $val) {
            $this->setHeader($key, $val);
        }
        return $this;
    }

    /**
     * @param string $name Имя хедера
     * @param string $value Значение хедера
     * @return $this
     */
    public function setHeader($name, $value)
    {
        $this->headers[$name] = $value;
        return $this;
    }

    /**
     * @param string $name Имя хедера
     * @return string|null
     */
    public function getHeader($name)
    {
        return $this->headers[$name];
    }

    /**
     * @param string $body тело json
     * @return $this
     */
    public function setBody($body)
    {
        $this->bodyParams = $body;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->bodyParams;
    }

    /**
     * Добавление POST данных
     *
     * @param string $name Название параметра
     * @param string $value Значение параметра
     * @return $this
     */
    public function addPost($name, $value)
    {
        $this->postParams[$name] = $value;
        return $this;
    }

    /**
     * Получение POST данных
     *
     * @param null|string $name Название параметра
     * @return array|mixed|null
     */
    public function getPost($name = null)
    {
        if ($name !== null) {
            return isset($this->postParams[$name]) ? $this->postParams[$name] : null;
        }
        return $this->postParams;
    }

    /**
     * Проверка наличия POST данных
     *
     * @return bool
     */
    public function hasPost()
    {
        return count($this->postParams) ? true : false;
    }

    /**
     * Очистить POST данные
     *
     * @return $this
     */
    public function clearPost()
    {
        $this->postParams = [];
        return $this;
    }

    /**
     * Очистить body данные
     *
     * @return $this
     */
    public function clearBody()
    {
        $this->setBody('');
        return $this;
    }
}