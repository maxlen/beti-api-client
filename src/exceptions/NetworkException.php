<?php

namespace Maxlen\BetiClient\exceptions;


/**
 * Класс исключения, вызываемый когдане удалось выполнить запрос
 *
 * @package Maxlen\BetiClient\exceptions
 */
class NetworkException extends BaseException
{
}