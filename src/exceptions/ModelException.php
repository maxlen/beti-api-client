<?php

namespace Maxlen\BetiClient\exceptions;


/**
 * Класс исключения, вызываемый когдане нет модели
 *
 * @package Maxlen\BetiClient\exceptions
 */
class ModelException extends BaseException
{
}