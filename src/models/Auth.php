<?php

namespace Maxlen\BetiClient\models;

use GuzzleHttp\Exception\ClientException;
use Maxlen\BetiClient\request\Params;
use Symfony\Component\HttpFoundation\Request;
use yii\web\JsonResponseFormatter;

/**
 * @see https://api.stage5.beti.io/swagger_ui/?urls.primaryName=auth
 * @package Maxlen\BetiClient\models
 */
class Auth extends BaseModel
{
    /**
     * @param array $bodyFields
     * @return $this
     */
    public function signin(array $bodyFields)
    {
        foreach (['brandId', 'department', 'device'] as $key) {
            $bodyFields[$key] = $bodyFields[$key] ?: $this->parameters->getParam($key);
        }

        $response = $this->setBody($bodyFields)->setMethod(Params::METHOD_POST)
            ->request('/auth/signin');

        if (isset($response['token'])) {
            $this->parameters->setParam('token', $response['token']);
        }

        if (isset($response['uuid'])) {
            $this->parameters->setParam('uuid', $response['uuid']);
        }

        $this->setBody([]);

        return $this;
    }

    /**
     * @param string $token
     * @return $this
     */
    public function logout($token = '')
    {
        if (!empty($token)) {
            $this->parameters->setParam('token', $token);
        }

        $response = $this->setMethod(Params::METHOD_POST)
            ->request('/auth/logout');

        return $this;
    }

    /**
     * @param string $token
     * @return $this
     */
    public function refreshToken($token = '')
    {
        $currToken = $token ?: $this->parameters->getParam('token');

        $response = $this->setMethod(Params::METHOD_GET)
            ->request("/auth/token/renew?token={$currToken}");

        if (isset($response['jwtToken'])) {
            $this->parameters->setParam('token', $response['jwtToken']);
        }

        return $this;
    }

    /**
     * @param $email
     * @return $this
     */
    public function passwordResetSendToken($email)
    {
        $brandId = $this->parameters->getParam('brandId');

        $response = $this->setBody(['email' => $email])->setMethod(Params::METHOD_POST)
            ->request("/auth/password/reset/request?brandId={$brandId}");

        $this->setBody([]);

        return $this;
    }

    /**
     * @param array $bodyFields ["password" => "", "repeatPassword" => "", "token": ""]
     * @return $this
     */
    public function passwordReset(array $bodyFields)
    {
        $response = $this->setBody($bodyFields)->setMethod(Params::METHOD_POST)
            ->request("/auth/password/reset");

        $this->setBody([]);

        return $this;
    }
}