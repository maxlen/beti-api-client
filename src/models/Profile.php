<?php

namespace Maxlen\BetiClient\models;

use Maxlen\BetiClient\models\defaults\ProfileSets;
use Maxlen\BetiClient\request\Params;

/**
 * @see https://api.stage5.beti.io/swagger_ui/?urls.primaryName=profile
 * @package Maxlen\BetiClient\models
 */
class Profile extends BaseModel
{
    /**
     * @param array $bodyFields (email, password)
     * @return $this
     */
    public function signup(array $bodyFields)
    {
        $bodyPreFields = ['brandId' => $bodyFields['brandId'] ?: $this->parameters->getParam('brandId')];
        $bodyFields = array_merge($bodyPreFields, $bodyFields);

        $response = $this->setBody($bodyFields)->setMethod(Params::METHOD_POST)
            ->request("/profile/public/signup?brandId={$bodyFields['brandId']}");

        if (isset($response['playerUUID'])) {
            $this->parameters->setParam('uuid', $response['playerUUID']);
        }

        return $this;
    }

    public function getProfile($uuid = '')
    {
        if (empty($uuid) && !empty($this->parameters->getParam('uuid'))) {
            $uuid = $this->parameters->getParam('uuid');
        }

        if (empty($uuid)) {
            return false;
        }

        $response = $this->setMethod(Params::METHOD_GET)
            ->request("/profile/profiles/{$uuid}");

        return $response;
    }
}