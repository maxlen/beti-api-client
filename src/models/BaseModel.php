<?php

namespace Maxlen\BetiClient\models;

use Maxlen\BetiClient\request\Params;
use \Maxlen\BetiClient\request\Request;

/**
 * Базовый класс моделей
 *
 * @package Maxlen\BetiClient
 */
abstract class BaseModel extends Request
{
    /**
     * Экземпляр Params для хранения аргументов
     * @var Params
     */
    public $parameters;

    /**
     * Конструктор моделей
     * @param array $parameters
     */
    public function __construct($parameters)
    {
        $this->parameters = $parameters;
        parent::__construct();
    }

    /**
     * Метод для добавления POST данных в запрос
     * @param array $fields
     */
    public function setFields($fields): self
    {
        foreach ($fields as $name => $value) {
            $this->parameters->addPost($name, $value);
        }

        return $this;
    }

    /**
     * Метод для добавления данных в тело запроса
     *
     * @param array $fields
     */
    public function setBody(array $fields, $asJson = true)
    {
        if ($asJson) {
            $fields = json_encode($fields);
        }

        $this->parameters->setBody($fields);
        return $this;
    }

    /**
     * @param string $method
     * GET, POST, PUT, DELETE ...
     */
    public function setMethod($method): self
    {
        $this->parameters->setMethod($method);
        return $this;
    }
}