<?php

namespace Maxlen\BetiClient\helpers;


class Convertor
{
    /**
     * Convert variable to boolean
     * @param $var
     * @return bool
     */
    public static function toBoolean($var)
    {
        if (is_bool($var)) {
            return $var;
        }

        if (is_int($var)) {
            return (bool)$var;
        }

        if (is_string($var)) {
            return (strtolower($var) == 'true') || ($var == '1');
        }

        if (is_array($var)) {
            return !empty($var);
        }

        if (is_object($var)) {
            return !empty((array) $var);
        }

        return false;
    }
}