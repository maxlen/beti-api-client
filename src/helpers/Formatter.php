<?php

namespace Maxlen\BetiClient\helpers;


/**
 * Класс для изменения формата данных в строках
 *
 * @package Maxlen\BetiClient\helpers
 */
class Formatter
{
    public static function toCamelCase($string)
    {
        return str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));
    }
}