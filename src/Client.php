<?php

namespace Maxlen\BetiClient;

use Maxlen\BetiClient\exceptions\ModelException;
use Maxlen\BetiClient\helpers\Formatter;
use Maxlen\BetiClient\request\Params;

/**
 * Class Client
 * @package Maxlen\BetiClient
 * @method BetiClient timeout(integer $value) set timeout in seconds (default 10s)

 * @property \GuzzleHttp\Psr7\Response $response
 * @property \Maxlen\BetiClient\models\Auth $auth
 * @property \Maxlen\BetiClient\models\Profile $profile
 */
class Client
{
    /**
     * @var Params|null Экземпляр Params для хранения аргументов
     */
    public $parameters;

    /**
     * @var \GuzzleHttp\Psr7\Response
     */
    public $response;

    public $debug = false;

    /**
     * BetiClient constructor.
     * @param array $params [
     *      'url' => '',
     *      'user' => '',
     *      'password' => ''
     *      'debug' => true | false
     *  ]
     */
    public function __construct(array $params = [])
    {
        $this->parameters = new Params();

        if (class_exists('Yii') && isset(\Yii::$app->params['beti-api'])) {
            foreach (\Yii::$app->params['beti-api'] as $key => $val) {
                $this->assertPropertyExist($key);
                $this->parameters->setParam($key, $val);
            }
        }

        if (!empty($params)) {
            foreach ($params as $key => $val) {
                $this->assertPropertyExist($key);
                $this->parameters->setParam($key, $val);
            }
        }
    }

    /**
     * Метод обеспечивающий взаимодействие с моделью
     *
     * @param string $name
     * @return mixed
     * @throws \denostr\Binotel\ModelException
     */
    public function __get($name)
    {
        $classname = 'Maxlen\\BetiClient\\models\\' . Formatter::toCamelCase($name);
        if (!class_exists($classname)) {
            throw new ModelException('Model not exists: ' . $name);
        }

        $this->parameters->clearPost();
        $this->parameters->clearBody();

        $model = new $classname($this->parameters);
        $this->response = &$model->response;

        return $model;
    }

    /**
     * @param string $name
     */
    protected function assertPropertyExist($name)
    {
        if (!property_exists(Params::class, $name)) {
            throw new \InvalidArgumentException("Undefined property - $name.");
        }
    }

//    private function validate()
//    {
//        $requiredFields = ['url', 'user', 'password', 'command'];
//
//        foreach ($requiredFields as $field) {
//            if (empty($this->$field)) {
//                throw new \Exception("{$field} is required! You should set it");
//            }
//        }
//
//        if (in_array($this->command, [Command::PUT_TO_PARSE]) && empty($this->options->action)) {
//            throw new \Exception("action is required! You should set it");
//        }
//
//        if (in_array($this->command, [Command::GET_TASK_STATUS, Command::GET_TASK_RESULT]) &&
//            empty($this->options->taskId)
//        ) {
//            throw new \Exception("taskId is required! You should set it");
//        }
//    }
}
